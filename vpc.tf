resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "terraform"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.0.0/24"
  
  availability_zone = "ap-northeast-2a"

  tags = {
    Name = "terraform-public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  availability_zone = "ap-northeast-2c"

  tags = {
    Name = "terraform-private-subnet"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  
  tags = {
    Name = "terraform-igw"
  }
}

resource "aws_eip" "nat" {
  vpc = true
  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat.id
  
  subnet_id = aws_subnet.public_subnet.id

  tags = {
    Name = "terraform-natgw"
  }
}



# route table 룰을 적용할 때 inner rule을 적용하는 방법과 새로운 리소스를 생성하는 두 가지 방법이 존재
# 새로운 리소스를 생성하여 route table 룰을 적용하는게 확장성이 더 뛰어남

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  # inner rule를 적용하는 방법
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "terraform-rt-public"
  }
}

resource "aws_route_table_association" "route_table_association_public" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  
  tags = {
    Name = "terraform-rt-private"
  }
}

resource "aws_route_table_association" "route_table_association_private" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private.id
}

# 새로운 리소스를 생성하여 route table 룰 적용하는 방법
resource "aws_route" "private_nat" {
  route_table_id              = aws_route_table.private.id
  destination_cidr_block      = "0.0.0.0/0"
  nat_gateway_id              = aws_nat_gateway.nat_gateway.id
}
